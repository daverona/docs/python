# Git repo as package repo

You can use a git repository as a private (or public) Python package repository.

Let's assume that `https://gitlab.com/daverona/templates/python.git` is the git repository of interest.

## Quick Start

### Listing 

To list all tags, branches, or both:

```bash
# using SSH
git ls-remote --tags --sort="-version:refname" ssh://git@gitlab.com/daverona/templates/python.git   # tags only
git ls-remote --heads --sort="-version:refname" ssh://git@gitlab.com/daverona/templates/python.git  # branches only
git ls-remote --refs --sort="-version:refname" ssh://git@gitlab.com/daverona/templates/python.git   # both
# using HTTPS
git ls-remote --tags --sort="-version:refname" https://gitlab.com/daverona/templates/python.git   # tags only
git ls-remote --heads --sort="-version:refname" https://gitlab.com/daverona/templates/python.git  # branches only
git ls-remote --refs --sort="-version:refname" https://gitlab.com/daverona/templates/python.git   # both
```

## Installation

To install a specific reference (tag or branch) or commit hash, say `ref`:

```bash
# using SSH
pip install git+ssh://git@gitlab.com/daverona/templates/python.git@ref
# using HTTPs
pip install git+https://gitlab.com/daverona/templates/python.git@ref
```

> Note that 
the last commit on master branch will be installed if `@ref` is omitted
and a short hash (the first 7 characters of a commit hash) may be used instead of a commit hash.

## References

* git ls-remote: [https://git-scm.com/docs/git-ls-remote.html](https://git-scm.com/docs/git-ls-remote.html)
* pypi installation: [https://www.linode.com/docs/applications/project-management/how-to-create-a-private-python-package-repository/](https://www.linode.com/docs/applications/project-management/how-to-create-a-private-python-package-repository/)
