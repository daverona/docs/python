# Python

## Tutorials

* Google's Python Class: [https://developers.google.com/edu/python/](https://developers.google.com/edu/python/)
* Dive Into Python 3: [https://www.cmi.ac.in/~madhavan/courses/prog3.0012/docs/diveintopython3/index.html](https://www.cmi.ac.in/~madhavan/courses/prog3.0012/docs/diveintopython3/index.html)
* Crash into Python: [https://stephensugden.com/crash\_into\_python/](https://stephensugden.com/crash_into_python/)
* Python 3 Tutorial: [https://youtu.be/Ozrduu2W9B8](https://youtu.be/Ozrduu2W9B8)
* The Python Tutorial: [https://docs.python.org/3/tutorial/index.html](https://docs.python.org/3/tutorial/index.html)

## Documentation

* Packaging Python Projects: [https://packaging.python.org/tutorials/packaging-projects/](https://packaging.python.org/tutorials/packaging-projects/)
* Packaging and distributing projects: [https://packaging.python.org/guides/distributing-packages-using-setuptools/](https://packaging.python.org/guides/distributing-packages-using-setuptools/)
* Packaging namespace packages: [https://packaging.python.org/guides/packaging-namespace-packages/](https://packaging.python.org/guides/packaging-namespace-packages/) 

## Style Guides

* PEP 8 -- Style Guide for Python Code: [https://www.python.org/dev/peps/pep-0008/](https://www.python.org/dev/peps/pep-0008/)
* Google Python Style Guide: [http://google.github.io/styleguide/pyguide.html](http://google.github.io/styleguide/pyguide.html)

## Linters

* pre-commit: [https://pre-commit.com/](https://pre-commit.com/)
* pre-commit Supported Hooks: [https://pre-commit.com/hooks.html](https://pre-commit.com/hooks.html)
* Read the Docs Settings: [https://github.com/readthedocs/common/tree/master](https://github.com/readthedocs/common/tree/master)
* Virtual Environments and Packages: [https://docs.python.org/3/tutorial/venv.html](https://docs.python.org/3/tutorial/venv.html)

## PyCharm

* Configure a remote interpreter using SSH: [https://www.jetbrains.com/help/pycharm/configuring-remote-interpreters-via-ssh.html](https://www.jetbrains.com/help/pycharm/configuring-remote-interpreters-via-ssh.html)
* Configure a remote interpreter using Docker: [https://www.jetbrains.com/help/pycharm/using-docker-as-a-remote-interpreter.html](https://www.jetbrains.com/help/pycharm/using-docker-as-a-remote-interpreter.html)
* Configure a remote interpreter using Docker Compose: [https://www.jetbrains.com/help/pycharm/using-docker-compose-as-a-remote-interpreter.html](https://www.jetbrains.com/help/pycharm/using-docker-compose-as-a-remote-interpreter.html)

## Packages

* celery: [https://pypi.org/project/celery/](https://pypi.org/project/celery/)
* gunicorn: [https://pypi.org/project/gunicorn/](https://pypi.org/project/gunicorn/)
* moviepy: [https://pypi.org/project/moviepy/](https://pypi.org/project/moviepy/)
* oauthlib: [https://pypi.org/project/oauthlib/](https://pypi.org/project/oauthlib/)
* paramiko: [https://pypi.org/project/paramiko/](https://pypi.org/project/paramiko/)
* phonenumbers: [https://pypi.org/project/phonenumbers/](https://pypi.org/project/phonenumbers/)
* pillow: [https://pypi.org/project/Pillow/](https://pypi.org/project/Pillow/)
* pycountry: [https://pypi.org/project/pycountry/](https://pypi.org/project/pycountry/)
* pygments: [https://pypi.org/project/Pygments/](https://pypi.org/project/Pygments/)
* python-decouple: [https://pypi.org/project/python-decouple/](https://pypi.org/project/python-decouple/)
* reportlab: [https://pypi.org/project/reportlab/](https://pypi.org/project/reportlab/)
* requests: [https://pypi.org/project/requests/](https://pypi.org/project/requests/)
* supervisor: [https://pypi.org/project/supervisor/](https://pypi.org/project/supervisor/)
* uvicorn: [https://pypi.org/project/uvicorn/](https://pypi.org/project/uvicorn/)

### Development Only

* pytest: [https://pypi.org/project/pytest/](https://pypi.org/project/pytest/)

## Troubleshooting

* Install MySQL and mysqlclient (Python) in macOS: [https://ruddra.com/posts/install-mysqlclient-macos/](https://ruddra.com/posts/install-mysqlclient-macos/)

## References

* A sample Python project: [https://github.com/pypa/sampleproject](https://github.com/pypa/sampleproject)
