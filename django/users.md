# Custom user model

> Note that you should do this right from the begining of your project.

Create an app:

```bash
python3 manage.py startapp users
```

> Note that you cannot use `auth` for app name.

Replace `users/models.py` with:

```python
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import Group as BaseGroup


class User(AbstractUser):
    # You may want to customize this part.
    pass


class Group(BaseGroup):
    pass
```

Replace `users/admin.py` with:

```python
from django.apps import apps
from django.contrib import admin
from django.contrib.auth.admin import GroupAdmin as BaseGroupAdmin
from django.contrib.auth.admin import UserAdmin
from .models import Group, User


BaseGroup = apps.get_model('auth.Group')
admin.site.unregister(BaseGroup)


class GroupAdmin(BaseGroupAdmin):
    pass


admin.site.register(User, UserAdmin)
admin.site.register(Group, GroupAdmin)
```

Replace `users/apps.py` with:

```python
from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class UsersConfig(AppConfig):
    name = 'users'
    verbose_name = _('Authentication and Authorization')
```

Add to `settings.py`:

```python
AUTH_USER_MODEL = 'users.User'
INSTALLED_APPS = [
    ...
    'users.apps.UsersConfig',
    ...
]
```

Make migrations and migrate:

```bash
python3 manage.py makemigrations
python3 manage.py migrate
```


## References

* https://docs.djangoproject.com/en/3.0/topics/auth/customizing/#substituting-a-custom-user-model
* https://wsvincent.com/django-tips-custom-user-model/
* https://medium.com/@ksarthak4ever/django-custom-user-model-allauth-for-oauth-20c84888c318  #noqa


