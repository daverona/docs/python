# Views



## Class-based views

### Dispatch methods

* `initial(self, request, *args, **kwargs)`
* `initialize_request(self, request, *args, **kwargs)`
* `finalize_response(self, request, response, *args, **kwargs)`
* `handle_exception(self, exc)`

## Function-based views

* `@api_view()`
* `@renderer_classes()`
* `@parser_classes()`
* `@authentication_classes()`
* `@throttle_classes()`
* `@permission_classes()`
* `@schema()`
