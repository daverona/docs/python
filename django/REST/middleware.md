# Django REST Middleware

* [https://crondev.blog/2018/05/13/django-middlewares-with-rest-framework/](https://crondev.blog/2018/05/13/django-middlewares-with-rest-framework/)

```python
from django.contrib.auth import get_user
from django.utils.functional import SimpleLazyObject
from .authentication import BearerAuthentication


class BearerAuthenticationMiddleware:
    @classmethod
    def get_user(cls, request):
        user = get_user(request)
        if not user.is_authenticated:
            authentication = BearerAuthentication()
            try:
                user, token = authentication.authenticate(request)
            except:
                pass
        return user

    def __init__(self, get_response):
        # Do one-time configuration and initialization.
        self.get_response = get_response

    def __call__(self, request):
        # Executed before the next middleware in the chain (or the view) processes the request.
        request.user = SimpleLazyObject(lambda: self.__class__.get_user(request))
        # Pass the request to the next middleware in the chain (or the view) and receive the response from it.
        response = self.get_response(request)
        # Executed after the next middleware in the chain (or the view) returns the response.
        return response
```