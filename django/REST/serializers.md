### Validation

```python
serializer.is_valid()

# will raise serializers.ValidationError
serializer.is_valid(raise_exception=True)

serializer.errors

class FooSerializer(serializers.Serializer):
    def validate_title(self, value):
        # return the validated value or raise a serializers.ValidationError
        pass

    def validate(self, data):
        # return the validated values or raise a serializers.ValidationError
        pass

```


