



* `StringRelatedField` is used to represent the target of the relationship using the `__str__` method.
* `PrimaryKeyRelatedField` is used to represent the target of the relationship using its primary key.
* `HyperlinkedRelatedField` is used to represent the target of the relationship using a hyperlink.
* `SlugRelatedField` is used to represent the target of the relationship using a field on the target.
* `HyperlinkedIdentityField` can be applied as an identity relationship or used for an attribute on the object.

## Nested relationships

## Custom relational fields

## Custom hyperlinked fields




