# Django

## Tutorials

* Getting Started: [https://docs.djangoproject.com/en/3.0/intro/](https://docs.djangoproject.com/en/3.0/intro/)
* Django Web Framework (Python): [https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django)
* Django REST Framework Tutorial: [https://www.django-rest-framework.org/tutorial/quickstart/](https://www.django-rest-framework.org/tutorial/quickstart/)
* Django Tutorial: [https://youtu.be/n-FTlQ7Djqc](https://youtu.be/n-FTlQ7Djqc)

## Documentation

* Django Documentation: [https://docs.djangoproject.com/en/3.0/](https://docs.djangoproject.com/en/3.0/)
* Django API Reference: [https://docs.djangoproject.com/en/3.0/ref/](https://docs.djangoproject.com/en/3.0/ref/)
* Django User's Manual: [https://docs.djangoproject.com/en/3.0/topics/](https://docs.djangoproject.com/en/3.0/topics/)
* Django REST Framework API Reference: [https://www.django-rest-framework.org/api-guide/requests/](https://www.django-rest-framework.org/api-guide/requests/)
* Django Packages: [https://djangopackages.org/](https://djangopackages.org/)
* Django Snippets: [https://djangosnippets.org/](https://djangosnippets.org/)
* How to write reusable apps: [https://docs.djangoproject.com/en/3.0/intro/reusable-apps/](https://docs.djangoproject.com/en/3.0/intro/reusable-apps/)
* My essential django package list: [https://spapas.github.io/2017/10/11/essential-django-packages/](https://spapas.github.io/2017/10/11/essential-django-packages/)
* Django Admin Cookbook: [https://books.agiliq.com/projects/django-admin-cookbook/en/latest/index.html](https://books.agiliq.com/projects/django-admin-cookbook/en/latest/index.html)

## Style Guides

* Django Python Coding Style: [https://docs.djangoproject.com/en/dev/internals/contributing/writing-code/coding-style/](https://docs.djangoproject.com/en/dev/internals/contributing/writing-code/coding-style/)
* Django JavaScript Coding Style: [https://docs.djangoproject.com/en/dev/internals/contributing/writing-code/javascript/](https://docs.djangoproject.com/en/dev/internals/contributing/writing-code/javascript/)

## Packages

* django-allauth: [https://pypi.org/project/django-allauth/](https://pypi.org/project/django-allauth/)
* django-compressor: [https://pypi.org/project/django-compressor/](https://pypi.org/project/django-compressor/)
* django-cors-headers: [https://pypi.org/project/django-cors-headers/](https://pypi.org/project/django-cors-headers/)
* django-csp: [https://pypi.org/project/django-csp/](https://pypi.org/project/django-csp/)
* django-filter: [https://pypi.org/project/django-filter/](https://pypi.org/project/django-filter/)
* django-guardian: [https://pypi.org/project/django-guardian/](https://pypi.org/project/django-guardian/)
* django-import-export: [https://pypi.org/project/django-import-export/](https://pypi.org/project/django-import-export/)
* django-phonenumber-field: [https://pypi.org/project/django-phonenumber-field/](https://pypi.org/project/django-phonenumber-field/)
* django-redis: [https://pypi.org/project/django-redis/](https://pypi.org/project/django-redis/)
* django-rest-auth: [https://pypi.org/project/django-rest-auth/](https://pypi.org/project/django-rest-auth/)
* django-reversion: [https://pypi.org/project/django-reversion/](https://pypi.org/project/django-reversion/)
* django-storage: [https://pypi.org/project/django-storages/](https://pypi.org/project/django-storages/)
* djangorestframework: [https://pypi.org/project/djangorestframework/](https://pypi.org/project/djangorestframework/)
* djangorestframework-api-key: [https://pypi.org/project/djangorestframework-api-key/](https://pypi.org/project/djangorestframework-api-key/)
* djangorestframework-guardian: [https://pypi.org/project/djangorestframework-guardian/](https://pypi.org/project/djangorestframework-guardian/)
* djangorestframework-simplejwt: [https://pypi.org/project/djangorestframework-simplejwt/](https://pypi.org/project/djangorestframework-simplejwt/)
* drf-nested-routers: [https://pypi.org/project/drf-nested-routers/](https://pypi.org/project/drf-nested-routers/)
* ~~jsonfield: [https://pypi.org/project/jsonfield/](https://pypi.org/project/jsonfield/)~~ (obsolete by [https://docs.djangoproject.com/en/3.1/ref/models/fields/#jsonfield](https://docs.djangoproject.com/en/3.1/ref/models/fields/#jsonfield))
* soft-delete: [https://pypi.org/project/soft-delete/](https://pypi.org/project/soft-delete/)

### Development Only

* django-debug-toolbar: [https://pypi.org/project/django-debug-toolbar/](https://pypi.org/project/django-debug-toolbar/)
* ~~django-extensions: [https://pypi.org/project/django-extensions/](https://pypi.org/project/django-extensions/)~~
* django-seed: [https://pypi.org/project/django-seed/](https://pypi.org/project/django-seed/)

## References

* Stack Overflow Developer Survey Results 2019: [https://insights.stackoverflow.com/survey/2019/](https://insights.stackoverflow.com/survey/2019/)
* Ruby on Rails vs Django vs Laravel: The Ultimate Comparison of Popular Web Frameworks:[https://steelkiwi.com/blog/ruby-django-laravel-frameworks-comparison/](https://steelkiwi.com/blog/ruby-django-laravel-frameworks-comparison/)
